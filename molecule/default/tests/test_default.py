import pytest
import os
import yaml
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture()
def AnsibleDefaults():
    with open("../../defaults/main.yml", 'r') as stream:
        return yaml.load(stream)


@pytest.mark.parametrize("dirs", [
    "/tmp/deployment_artefacts",
    "/etc/ansible/facts.d",
    "/opt/coremedia/studio/server-lib",
    "/opt/coremedia/studio/common-lib",
    "/opt/coremedia/studio/current/webapps/studio/WEB-INF/properties/corem",
    "/var/cache/coremedia/studio",
    "/var/cache/coremedia/studio/persistent-transformed-blobcache"

])
def test_directories(host, dirs):
    d = host.file(dirs)
    assert d.is_directory
    assert d.exists


@pytest.mark.parametrize("files", [
    "/etc/ansible/facts.d/studio.fact",
    "/opt/coremedia/studio/common-lib/coremedia-tomcat.jar",
    "/opt/coremedia/studio/current/bin/setenv.sh",
    "/opt/coremedia/studio/studio.properties"
])
def test_files(host, files):
    f = host.file(files)
    assert f.exists
    assert f.is_file


def test_user(host):
    assert host.user("coremedia").exists
    assert host.user("studio").exists
    assert host.group("coremedia").exists
