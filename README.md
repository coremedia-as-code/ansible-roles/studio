# CoreMedia - studio

## Ports

```
41005
41099
41098
41080
41009
```

## dependent services

### solr
```
  elastic:
    social:
      solr:
        url: http://127.0.0.1:40080/solr
```

### content-management-server
```
elastic_worker:
  repository:
    url: http://content-management-server.int:40180/content-management-server/ior
```
